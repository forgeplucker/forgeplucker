
import RDF

try:
    import json
except ImportError:
    import simplejson as json


class OSLCCoreJsonRDFParser(RDF.Parser):
    
  def __init__(self):
    """Create an RDF Parser for OSLC Core compatible JSON format (constructor).

    OSLC (Open Services for Lifecycle Collaboration) specifications (OSLC Core V2) defines guidelines for RDF representation in JSON
     
    """
    
    #global _debug    
    #if _debug:
    #  print "Creating RDF.Parser name=oslccorejson"
    namespaces = {}

  def __del__(self):
      pass

  def parse_as_stream(self, uri, base_uri=None):
    """Return a Stream of Statements from parsing the content at
        a URI for the optional base URI or None if the parsing fails.

        (This depends on what URI support raptor provides to redland)
        
          for statement in parser.parse_as_stream("http://localhost/r.rdf"):
              print statement
    """
    print "OSLCCoreJsonRDFParser:parse_as_stream : ERROR, TO BE IMPLEMENTED"
    sys.exit(1)
    
  def parse_string_as_stream(self, string, base_uri):
    """Return a Stream of Statements from parsing the content in
        string with the required base URI or None if the parsing fails.

          for statement in parser.parse_string_as_stream(rdfstring, base_uri):
              print statement
    """
    print "OSLCCoreJsonRDFParser:parse_string_as_stream ERROR, TO BE IMPLEMENTED"
    sys.exit(1)

  def parse_into_model(self, model, uri, base_uri=None, handler=None):
    """Parse into the Model model from the content at the URI, for
       the optional base URI.  Returns a boolean success or failure.
        
       If handler is given, an error handler with the signature
         def handler(code, level, facility, message, line, column, byte, file, uri)
       is called.

       parser.parse_into_model(model, "file:./foo.rdf",
                               "http://example.com/foo.rdf")
    """
    print "OSLCCoreJsonRDFParser:parse_into_model ERROR, TO BE IMPLEMENTED"
    sys.exit(1)

    
  def namespaces_seen(self):
    """Get a dictionary of prefix/URI pairs for namespaces seen during parsing.
"""
    return self.prefixes
  
  def adddict_to_model(self, model, resource_uri, key, value):
    "Convenience class for recursive tree decoding : decodes contents of a dictionary and adds it to the model"
    subject = RDF.Uri(resource_uri)
    (pref, suf) = key.split(':')
    predicate_uri = self.prefixes[pref] + suf
    object = None
    if not 'rdf:about' in value:
        bnode = RDF.Node()
        #print "bnode", bnode
        object = bnode
    else:    
        object_uri = value['rdf:about']
        object = RDF.Uri(object_uri)
        del value['rdf:about']
        print resource_uri, predicate_uri, object_uri
        
    model.append(RDF.Statement(subject, RDF.Uri(predicate_uri), object))
    object_uri = str(object)
    
    if 'prefixes' in value:
        for p in value['prefixes'].keys():
            self.prefixes[p] = value['prefixes'][p]
        del value['prefixes']
    for k in value.keys():
        val = value[k]
        
        if isinstance(val, dict):
            self.adddict_to_model(model, object_uri, k, val)
            
        elif isinstance(val, list):
            for item in val:
                if isinstance(item, dict):
                    self.adddict_to_model(model, object_uri, k, item)
                    
        elif isinstance(val, unicode):
            (pref, suf) = k.split(':')
            if suf:
                if pref in self.prefixes:
                    predicate_uri = self.prefixes[pref] + suf
                    model.append(RDF.Statement(RDF.Uri(object_uri), RDF.Uri(predicate_uri), val))
                else:
                    print "PBM:", k, type(val)
            else:
                print "PBM", k, type(val)
                
        elif isinstance(val, int):
            (pref, suf) = k.split(':')
            if suf:
                if pref in self.prefixes:
                    predicate_uri = self.prefixes[pref] + suf
                    model.append(RDF.Statement(RDF.Uri(object_uri), RDF.Uri(predicate_uri), str(val)))
                else:
                    print "PBM", k, type(val)
            else:
                print "PBM", k, type(val)
        else:
            print "PBM", k, type(val)
        
  def parse_string_into_model(self, model, string, base_uri, handler = None):
    """Parse into the Model model from the content string
        with the required base URI.  Returns a boolean success or failure.

       If handler is given, an error handler with the signature
         def handler(code, level, facility, message, line, column, byte, file, uri)
       is called.

    """
    if type(base_uri) is str:
      base_uri = Uri(string = base_uri)
    elif type(base_uri) is unicode:
      import Redland_python
      base_uri = Uri(string=Redland_python.unicode_to_bytes(base_uri))
    if base_uri is None:
      raise RedlandError("A base URI is required when parsing a string")

    if handler is not None:
      import Redland_python
      Redland_python.set_callback(handler)

    if type(string) is unicode:
      import Redland_python
      string=Redland_python.unicode_to_bytes(string)

    #rc=Redland.librdf_parser_parse_string_into_model(self._parser, string,
    #                                       base_uri._reduri, model._model)
    print string
    
    data = json.loads(string)
    print data
    self.prefixes = data['prefixes']
    del data['prefixes']
    
    resource_uri = data['rdf:about']
    print resource_uri
    del data['rdf:about']
    resource_type = data['rdf:type']
    print "resource_type:", resource_type
    del data['rdf:type']
    
    rdf_type = "rdf:type"
    (pref, suf) = rdf_type.split(':')
    predicate_uri = self.prefixes[pref] + suf
    model.append(RDF.Statement(RDF.Uri(resource_uri), RDF.Uri(predicate_uri), RDF.Uri(resource_type)))
    
    print data.keys()
    for key in data.keys():
        value = data[key]
        if isinstance(value, dict):
            self.adddict_to_model(model, resource_uri, key, value)
            
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    self.adddict_to_model(model, resource_uri, key, item)
                
        else:
            print "PBM", key, type(value)
        
    
    if handler is not None:
      import Redland_python
      Redland_python.reset_callback()

    rc=True
    return (rc != None)
